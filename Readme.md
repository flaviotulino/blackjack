# BlackJack

### Install
Please run npm install to install all the project dependencies. They are:

- webpack, used to bundled all the components in one file
- webpack-server-dev, used to run a light server
- scss
- node cuncurrently, used to run webpack, webpack server and scss watchersm to automatically refresh the page when something changes

### Contents
- package.json
- webpack.config.js
- node_modules *(_BE SURE YOU RUN THE INSTALL FIRST!_)*
- app/components, es6 components
- app/stylesheets, some scss modules


### Run
To run the project, just type `npm start` in your terminal and visit http://localhost:8080/app from your browser
