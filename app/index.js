import {Deck} from './components/Deck';
import {PlayerGame} from './components/PlayerGame';
import {DialerGame} from './components/DialerGame';

// prepare the deck injecting the 52 cards
Deck.prepare();

// shuffle the cards
let cards = Deck.shuffle();

// set the games passing their containers selectors
let dialerGame = new DialerGame('#dialer-game','#dialer-points');
let playerGame = new PlayerGame('#player-game','#player-points');

// give two cards to the dialer and two cards to the player
for (let i = 0; i < 2; i++) {
  let card = Deck.hit();
  dialerGame.card(card);
}

/* <b>Please Note:</b> of course this for could be combined with the one above,
*  but the goal is to simulate a real round
*/
for (let i = 0; i < 2; i++) {
  let card = Deck.hit();
  playerGame.card(card);
}

// the player does Blackjack! (21 in one shot)
if (playerGame.points == 21) {
  $('#player-points').text('Blackjack!');
  $("#hit").attr('disabled','disabled');
  dialerTurn();
}

$('#hit').on('click', () => {
  playerGame.card(Deck.hit())

  // the player exceeded or reached 21. In both case, it's the dialer turn
  if (playerGame.points >= 21) {
    $("#hit").attr('disabled','disabled');
    dialerTurn();
  }
})

$('#stay').on('click', () => {
  $("#hit").attr('disabled','disabled');
  $("#stay").attr('disabled','disabled');

  dialerTurn();
})

/** Thi simple function handles the dialer turn */
function dialerTurn() {
  // show the dialer cards and his points
  $('.container').removeClass('unrevealed');

  // if the player exceeded
  if (playerGame.points > 21) {

    // set a label
    playerGame.exceeded();

    // tell the dialer wins
    dialerGame.wins();
  } else {
    // dialer hit cards until reaches the player's points
    while (dialerGame.points <= playerGame.points && dialerGame.points < 21) {
      dialerGame.card(Deck.hit())
    }

    // if the dialer exceeded
    if (dialerGame.points > 21) {

      // set a label
      dialerGame.exceeded();

      // tell the player wins
      playerGame.wins();
    }
    // the dialer has more points than the player
    else if (dialerGame.points > playerGame.points) {
      dialerGame.wins();
    }
    // dialer and the player have the same points or both exceeded (this case looks impossible, btw)
    else if (dialerGame.points == playerGame.points || dialerGame.points > 21 && playerGame.points > 21) {

      // tell player and dialer draw
      $('#winner-container').text("Draw")
    }
  }
}
