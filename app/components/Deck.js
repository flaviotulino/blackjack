import {Card,SEEDS} from './Card';

/**
  * This class handles the deck prepare, shuffling and the hitting of a new card
  */
export class Deck {
  getCards() {
    return Deck.cards;
  }

  /**
    * Make this class as a singleton. Just in case
    */
  static getInstance() {
    if (Deck.__instance == undefined) {
      Deck.__instance = new Deck();
    }
    return Deck.__instance;
  }

  /**
    * Prepare the deck pushing cards and setting their values, symbols and seeds
    */
  static prepare() {
    Deck.cards = [];

    // for each seed defined in the Card class
    Object.keys(SEEDS).map(seed => {

      // for the first 10 cards
      for (let i = 1; i < 11; i++) {
        // if is the first card just set 'A' as symbol for the ace
        if (i == 1) {
          Deck.cards.push(new Card(1,seed,'A'));
        } else {
          // pushing the other cards. Their values and symbols are identical
          Deck.cards.push(new Card(i, seed));

          // for the last three cards
          if (i == 10) {
            // the value is still 10, but the symbols using J,Q,K
            Deck.cards.push(new Card(10,seed,'J'));
            Deck.cards.push(new Card(10,seed,'Q'));
            Deck.cards.push(new Card(10,seed,'K'))
          }
        }
      }
    });
    return Deck.__instance;
  }

  /**
    * Just randomized the prepared deck
    */
  static shuffle() {
    Deck.cards.sort(function() { return 0.5 - Math.random() });
    return Deck.cards;
  }

  /**
    * Remove the hit card
    */
  static hit() {
    let c = Deck.cards[Deck.cards.length -1];
    Deck.cards.pop();
    return c;
  }
}
