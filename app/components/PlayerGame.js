import {Game} from './Game';

/**
* This class inherits from Game in order to behave slightly differently
*/
export class PlayerGame extends Game {
  /**
  * Draw the winner
  */
  wins() {
    $("#winner-container").text('Player Wins');
  }

  /**
  * Share the same behaviour with the super class, and show the current points
  * @param card the hit card
  */
  card(card) {
    super.card(card);

    // show and update the player points
    $(this.pointsContainer).text(this.points);
  }
}
