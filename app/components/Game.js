/**
* This class handles the entire game, giving a new card when requested and
* set the game as exceeded.
* Calculates the points adapting the value of the ace with 1 or 11.
*/
export class Game {
  /**
  * @constructor
  * @param container is the game container selector
  * @param pointsContainer is the points container selector
  */
  constructor(container,pointsContainer) {
    this.container = container;
    this.pointsContainer = pointsContainer;

    // init empty cards array
    this.cards = [];
  }

  /**
  * Draw the card on the dashboard
  * @param card the hit card from the deck
  */
  card(card) {
    // add the card to the array
    this.cards.push(card);

    // create a card container setting the seed class modifier
    let $card = $('<div></div>');
    $card.addClass('card').addClass(`card--${card.seed.toLowerCase()}`);

    // display the card seed in its center
    let $card__seed = $('<div></div>').addClass('card__seed').text(card.icon);
    $card.append($card__seed);

    // decorate the card drawing the card symbol and the icon in the 4 borders
    for (let i = 0; i < 4; i++) {
      let $card__label = $("<div></div>").addClass('card__label').text(card.symbol + card.icon);
      $card.append($card__label);
    }

    // append the card in the dashboard
    $(this.container).append($card);
  }

  /**
  * The dialer/player exceeded. Just show a label
  */
  exceeded () {
    $(this.pointsContainer).append("<span class='exceeded'>EXCEDEED</span>");
    $("#hit").attr('disabled','disabled');
  }

  /**
  * A getter which calculates the player/dialer points
  */
  get points () {
    /*
    * Ace can have two different values: 1 and 11.
    * In order to achieve the best fit value, calculate the points separately.
    */
    let noAces = this.cards.filter((c) => {return c.value != 1});
    let aces = this.cards.filter((c) => {return c.value == 1});

    let points = 0;

    // if the player/dialer has some cards different from ace
    if (noAces.length > 0) {
      // calculate the cards points sum
      points = noAces.reduce(function (acc,obj) {
        return (acc + obj.value)
      },0);
    }

    // if the player/dialer has some aces
    if (aces.length > 0){
      aces.map((a) => {
        // if the ace can be an 11 without exceed, consider it as an 11
        if (points + 11 <= 21) {
          points += 11;
        } else {
          // or just consider it as a 1
          points += 1;
        }
      })
    }

    return points;
  }
}
