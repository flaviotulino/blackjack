import {Game} from './Game'

/**
* This class inherits from Game in order to behave slightly differently
*/
export class DialerGame extends Game {
  /**
  * Draw the winner
  */
  wins() {
    $("#winner-container").text('Dialer Wins');

    // be sure to display the points
    $(this.pointsContainer).text(this.points);
  }

  /**
  * Share the same behaviour with the super class, but hide the dialer points
  * while the player is still playing
  * @param card the hit card
  */
  card(card) {
    super.card(card);

    // show "***" instead of points while is still the player's turn
    $(this.pointsContainer).text($('.container').hasClass('unrevealed') ? '***' : this.points);
  }
}
