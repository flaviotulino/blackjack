/**
  * This class is used in order to show cards inside the game
  */
export class Card {
  /**
    * @constructor
    * @param value is the value of the card, from 1 to 10
    * @param seed is the seed of the card taken from a list of four seeds
    * @param symbol <optional> is used to display the card value. 1 is displayed
    *        as 'A' (ace), the three cards above 10 are displayed as 'J','Q','K'
    */
  constructor(value, seed,symbol) {
    this.value = value;
    this.seed = seed;
    this.icon = SEEDS[seed];
    this.symbol = symbol || value.toString();
  }
}

/**
  * A list of available seeds within their unicode icon
  */
export const SEEDS = {
  HEARTS: '♥',
  DIAMONDS: '♦',
  SPADES: '♠',
  CLUBS: '♣'
};
