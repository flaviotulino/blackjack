var webpack = require('webpack');

module.exports = {
  context: __dirname,
  entry: "./app/index.js",
  output: {
    path: __dirname + "/js",
    filename: "scripts.min.js"
  }
};
